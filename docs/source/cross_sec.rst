cross_sec command
*****************

.. automodule:: rmt_utilities.cross_sec
.. argparse::
   :module: rmt_utilities.cross_sec
   :func: read_command_line
   :prog: cross_sec

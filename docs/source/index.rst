.. Packaging Scientific Python documentation master file, created by
   sphinx-quickstart on Thu Jun 28 12:35:56 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. include::  ../../README.rst

.. toctree::
   :maxdepth: 1

   installation
   usage
   release-history
   min_versions
   autoapi/index
   cmd
   RMT documentation <https://uk-amor.gitlab.io/RMT/rmt>

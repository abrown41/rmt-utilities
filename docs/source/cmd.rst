Command line utilities
**********************

The help string for individual command line utilities can be viewed by executing
the command with the `-h` or `--help` option, e.g.

.. code-block:: bash
   
      >>> rmt_compare --help
      >>> gen_hhg -h

.. toctree::
   :maxdepth: 1

   gen_hhg
   gen_tas
   plot_mom
   plot_den
   rmt_compare
   cross_sec

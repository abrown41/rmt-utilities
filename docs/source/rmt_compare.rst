.. _rmt_compare:

rmt_compare command
***********************

.. automodule:: rmt_utilities.rmt_compare
.. argparse::
   :module: rmt_utilities.rmt_compare
   :func: read_command_line
   :prog: rmt_compare

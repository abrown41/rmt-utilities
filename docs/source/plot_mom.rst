plot_mom command
***********************

.. automodule:: rmt_utilities.plot_dist.plot_mom
.. argparse::
   :module: rmt_utilities.reform_cli
   :func: momentum_command_line
   :prog: plot_mom


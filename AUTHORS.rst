=======
Credits
=======

Maintainer
----------

* Andrew Brown <andrew.brown@qub.ac.uk>

Contributors
------------

None yet. Why not be the first? See: CONTRIBUTING.rst

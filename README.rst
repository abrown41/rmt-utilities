=============
RMT utilities
=============

.. image:: https://gitlab.com/abrown41/rmt-utilities/badges/master/pipeline.svg
        :target: https://gitlab.com/abrown41/rmt-utilities/-/commits/master

.. image:: https://gitlab.com/abrown41/rmt-utilities/badges/master/coverage.svg
        :target: https://gitlab.com/abrown41/rmt-utilities/-/commits/master

.. image:: https://img.shields.io/pypi/v/rmt-utilities.svg
        :target: https://pypi.python.org/pypi/rmt-utilities


Python package for interacting with the high performance, parallel fortran code R-matrix with Time `(RMT) <https://gitlab.com/Uk-amor/RMT/rmt>`_


* Free software: `GPL-v3 <https://www.gnu.org/licenses/gpl-3.0.en.html>`_
* `Package repository <https://gitlab.com/abrown41/rmt-utilities>`_

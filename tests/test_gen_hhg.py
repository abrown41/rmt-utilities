import pytest
import unittest
from rmt_utilities.dipole_cli import get_command_line as gcl
from rmt_utilities.gen_hhg import get_hhg_spectrum as ghg
from bunch import Bunch
import os
import matplotlib.pyplot as plt


dummyargs = {'x': False,
             'y': False,
             'z': True,
             'plot': False,
             'output': True,
             'pad_factor': 8,
             'solutions_list': None,
             'units': 'eV'}


class genhhg(unittest.TestCase):
    @classmethod
    def setUp(self):
        from subprocess import call
        call(['mkdir', 'tests/spawn'])
        call(['cp', 'tests/data/expec_z_all.neon00001000', 'tests/spawn/'])
        call(['cp', 'tests/data/expec_z_all.neon00001000',
              'tests/spawn/expec_v_all.neon00001000'])

    @classmethod
    def tearDown(self):
        from subprocess import call
        call(['rm', '-rf', 'tests/spawn'])

    @pytest.mark.slow
    def test_gcl_defaults(self):
        """This test isn't actually slow, but breaks if the --skip_slow argument
        is passed"""
        args = gcl('harmonic')
        assert args.files == ['.']
        for arg in dummyargs:
            assert getattr(args, arg) == dummyargs[arg]

    def test_get_hhg(self):
        newargs = Bunch(dummyargs)
        newargs.files = ['tests/spawn']
        ax = ghg(newargs)
        assert ax
        plt.close()

    def test_output(self):
        newargs = Bunch(dummyargs)
        newargs.files = ['tests/spawn']
        ghg(newargs)
        assert os.path.isfile('tests/spawn/Harm_len_0001')
        assert os.path.isfile('tests/spawn/Harm_len_0002')
        assert os.path.isfile('tests/spawn/Harm_vel_0001')
        assert os.path.isfile('tests/spawn/Harm_vel_0002')
        plt.close()

    def test_plot(self):
        newargs = Bunch(dummyargs)
        newargs.files = ['tests/spawn']
        newargs.plot = True
        newargs.output = False
        plt.ion()
        ax = ghg(newargs)
        assert len(ax.get_lines()) == 4
        assert ax.get_xlabel() == 'Frequency (eV)'
        plt.close()

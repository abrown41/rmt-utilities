import pytest
import unittest
import rmt_utilities.reform_cli as rcli
import rmt_utilities.plot_dist as rpd
from bunch import Bunch
import os
import matplotlib.pyplot as plt

plt.ion()

myrootdir = 'tests/spawn'
targetfile = f'{myrootdir}/OuterWave_density.txt'
defaultargs = {'dir': '../',
               'output': None,
               'plot': False,
               'normalise_bar': False,
               'log_scale': False,
               'rmax': None}

dummyargs = Bunch({'file': targetfile,
                   'dir': myrootdir,
                   'output': f'{myrootdir}/op.png',
                   'plot': True,
                   'normalise_bar': True,
                   'log_scale': True,
                   'rmax': 1.5})


class testPlotDist(unittest.TestCase):
    @classmethod
    def setUp(self):
        from subprocess import call
        call(['mkdir', myrootdir])
        call(['cp', 'tests/data/state/OuterWave_density.txt', myrootdir])
        call(['cp', 'tests/data/state/input.conf', myrootdir])

    @classmethod
    def tearDown(self):
        from subprocess import call
        call(['rm', '-rf', 'tests/spawn'])

    def test_gcl_defaults(self):
        parser = rcli.read_command_line('momentum distribution')
        assert parser
        args = parser.parse_args([targetfile])
        for arg in defaultargs:
            assert getattr(args, arg) == defaultargs[arg]

    def test_momentum_cli(self):
        parser = rcli.momentum_command_line()
        assert parser
        args = parser.parse_args([targetfile])
        momargs = defaultargs.copy()
        momargs['rskip'] = 200
        for arg in momargs:
            assert getattr(args, arg) == momargs[arg]

    def test_density_cli(self):
        parser = rcli.density_command_line()
        assert parser
        args = parser.parse_args([targetfile])
        denargs = defaultargs.copy()
        denargs['rmatr'] = 20.0
        for arg in denargs:
            assert getattr(args, arg) == denargs[arg]

    @pytest.mark.slow
    def test_plot_mom(self):
        inargs = Bunch(dummyargs)
        inargs.rskip = 200
        inargs.output = f'{myrootdir}/momdist.png'
        ax = rpd.plot_dist(inargs, 'momentum distribution')
        assert ax
        assert ax.get_rmax() == inargs.rmax
        assert os.path.isfile(inargs.output)
        plt.close()

    @pytest.mark.slow
    def test_plot_den(self):
        inargs = Bunch(dummyargs)
        inargs.rmatr = None
        inargs.output = f'{myrootdir}/dendist.png'
        ax = rpd.plot_dist(inargs, 'electron density')
        assert ax
        assert ax.get_rmax() == 20
        assert ax.get_rmin() == inargs.rmax
        assert os.path.isfile(inargs.output)
        plt.close()

    def test_set_default_output(self):
        dargs = Bunch({'plot': False,
                      'output': False})
        newargs = rcli.set_default_output_file(dargs, 'this and that')
        assert newargs.output == 'this_and_that.png'

    def test_select_momentum(self):
        parser = rcli.select_command_line('momentum distribution')
        args = parser.parse_args([targetfile])
        assert (args.rskip == 200)
        assert (not hasattr(args, 'rmatr'))

    def test_select_density(self):
        parser = rcli.select_command_line('electron density')
        args = parser.parse_args([targetfile])
        assert (args.rmatr == 20)
        assert (not hasattr(args, 'rskip'))

    def test_select_other(self):
        parser = rcli.select_command_line('googlyboo')
        args = parser.parse_args([targetfile])
        assert (not hasattr(args, 'rskip'))
        assert (not hasattr(args, 'rmatr'))

    def test_noconfig(self):
        with pytest.raises(IOError):
            args = Bunch({'dir': '.'})
            rpd.plot_dist(args, 'thisandthat')

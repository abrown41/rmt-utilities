import unittest
import rmt_utilities.rmt_compare as rmc
from bunch import Bunch
import io
import contextlib


goodargs = {'fileA': 'tests/goodcalc',
            'fileB': 'tests/data',
            'tolerance': 9,
            'quiet': False}

badargs = {'fileA': 'tests/badcalc',
           'fileB': 'tests/data',
           'tolerance': 9,
           'quiet': False}


class rmt_compare_tests(unittest.TestCase):
    @classmethod
    def setUp(self):
        from subprocess import call
        call(['cp', '-r', 'tests/data/', 'tests/goodcalc'])
        call(['cp', '-r', 'tests/data/', 'tests/badcalc'])
        with open('tests/goodcalc/expec_z_all.neon00001000', 'r') as f:
            with open('tests/badcalc/expec_z_all.neon00001000', 'w') as g:
                for line in f.readlines():
                    g.writelines(line.replace(' 0.0 ', ' 0.0000001 '))

    @classmethod
    def tearDown(self):
        from subprocess import call
        call(['rm', '-rf', 'tests/goodcalc/'])
        call(['rm', '-rf', 'tests/badcalc/'])

    def test_goodcalc(self):
        nargs = Bunch(goodargs)
        f = io.StringIO()
        with contextlib.redirect_stdout(f):
            assert rmc.compare(nargs)
        assert f.getvalue().startswith('RMT calculation in:')
        assert 'tests/goodcalc' in f.getvalue()

    def test_badcalc(self):
        nargs = Bunch(badargs)
        f = io.StringIO()
        with contextlib.redirect_stdout(f):
            try:
                rmc.compare(nargs)
            except SystemExit as e:
                assert e.__str__().startswith('Calculations do not agree')
        assert f.getvalue().startswith('RMT calculation in:')

    def test_calcs_equal(self):
        from rmt_utilities.rmtutil import RMTCalc
        calcA = RMTCalc('tests/data')
        calcB = RMTCalc('tests/goodcalc')
        calcC = RMTCalc('tests/badcalc')
        assert calcA == calcB
        assert calcA != calcC

    def test_quiet(self):
        nargs = Bunch(badargs)
        nargs.quiet = True
        f = io.StringIO()
        with contextlib.redirect_stdout(f):
            try:
                rmc.compare(nargs)
            except SystemExit as e:
                assert e.__str__().startswith('Calculations do not agree')
        assert f.getvalue() == ""

    def test_tolerance(self):
        nargs = Bunch(badargs)
        nargs.tolerance = 6
        assert rmc.compare(nargs)

    def test_args(self):
        parser = rmc.read_command_line()
        assert parser
        args = parser.parse_args(['tests/goodcalc', 'tests/data'])
        for arg in goodargs:
            assert getattr(args, arg) == goodargs[arg]

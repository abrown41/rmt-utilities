def build_popchn():
    import numpy as np
    dt = np.dtype([('params', [(f'{x}', np.int32) for x in range(5, 0, -1)]),
                   ('deltat', np.float64),
                   ('data', [(f'{x}', np.float64) for x in range(120)])])
    x = np.zeros((1,), dtype=dt)
    for y in range(1, 6):
        x['params'][f'{y}'] = y
    x['deltat'] = 0.01
    for ii, y in enumerate(np.linspace(0, 1, 120)):
        x['data'][f'{ii}'] = y

    x.tofile('tests/data/popchn.dat')


def erase_popchn():
    from subprocess import call
    call(["rm", "tests/data/popchn.dat"])

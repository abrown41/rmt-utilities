#!/bin/sh
# simulate an calculation by creating a selection of output files

touch tests/spawn/pop_{all,inn,out}.out
touch tests/spawn/data/pop_GS.out
touch tests/spawn/{ground,state}/psi_{inn,out}er.out

import unittest
import pytest
import rmt_utilities.data_recon as dr
import rmt_utilities.dataobjects as ru
import numpy as np
from pathlib import Path
from subprocess import call


class recond_tests(unittest.TestCase):
    @classmethod
    def setUp(self):
        dt = np.dtype([('params', [(f'{x}', np.int32) for x in range(5, 0, -1)]),
                       ('deltat', np.float64),
                       ('data', [(f'{x}', np.float64) for x in range(120)])])
        x = np.zeros((1,), dtype=dt)
        for y in range(1, 6):
            x['params'][f'{y}'] = y
        x['deltat'] = 0.01
        for ii, y in enumerate(np.linspace(0, 1, 120)):
            x['data'][f'{ii}'] = y

        x.tofile('tests/data/popchn.dat')
        self.pp = ru.popdata('./tests/data/popchn.dat')

    @classmethod
    def tearDown(self):
        call(["rm", "tests/data/popchn.dat"])

    def test_args(self):
        parser = dr.read_command_line()
        assert parser
        args = parser.parse_args(['this'])
        assert args.popchn == 'this'
        args = parser.parse_args([])
        assert args.popchn is None

    def test_argument_supplied(self):
        parser = dr.read_command_line()
        args = parser.parse_args(['googlyboo'])
        with pytest.raises(FileNotFoundError):
            dr.get_popchn_file(args)
        args = parser.parse_args(['tests/data'])
        assert (dr.get_popchn_file(args) == Path('./tests/data/popchn.dat'))
        args = parser.parse_args(['tests/data/popchn.dat'])
        assert (dr.get_popchn_file(args) == Path('./tests/data/popchn.dat'))

    def test_no_arg_supplied(self):
        parser = dr.read_command_line()
        args = parser.parse_args([])
        with pytest.raises(FileNotFoundError):
            dr.get_popchn_file(args)
        call(['touch', './popchn.1'])
        assert dr.get_popchn_file(args) == Path('./popchn.1')
        call(['rm', '-rf', './popchn.1'])

    def test_find_popchn(self):
        with pytest.raises(FileNotFoundError):
            dr.find_popchn_file(Path('./'))
        assert dr.find_popchn_file(Path('./tests/data/')) == Path('./tests/data/popchn.dat')
        call(['touch', './tests/popchn.1', './tests/popchn.2'])
        with pytest.warns(UserWarning):
            assert dr.find_popchn_file(Path('tests/')) == Path('tests/popchn.1')
        call(['rm', '-rf', './tests/popchn.1', './tests/popchn.2'])

    @pytest.mark.filterwarnings("ignore")
    def test_recon(self):
        with pytest.raises(Exception):
            dr.recon()

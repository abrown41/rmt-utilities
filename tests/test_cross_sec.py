import pytest
import unittest
import rmt_utilities.cross_sec as cs
from subprocess import call

dummyargs = {'files': ['.'],
             'nphotons': 1,
             'channels': ['1'],
             'sols': None}


class cross_sec_Tests(unittest.TestCase):
    @classmethod
    def setup_class(self):
        from tests.setup import build_popchn
        build_popchn()
        call(['mv', './tests/data/popchn.dat',
              './tests/data/popchn.00001000'])
        with open('./tests/input.conf', 'w') as ipf:
            ipf.write("""&InputData
                         version_root = ""
                         periods_of_ramp_on = 0
                         periods_of_pulse = 1
                         frequency = 1
                         intensity = 1
                         /""")

    @classmethod
    def teardown_class(self):
        call(["rm", "./tests/data/popchn.00001000"])
        call(["rm", "./tests/input.conf"])

    @pytest.mark.slow
    def test_defaults(self):
        """This test isn't actually slow, but breaks if the --skip_slow argument
        is passed"""
        parser = cs.read_command_line()
        args = parser.parse_args()
        print(args)
        for arg in dummyargs:
            assert getattr(args, arg) == dummyargs[arg]

    def test_cs(self):
        from bunch import Bunch
        import io
        import contextlib
        args = Bunch(dummyargs)
        args.files = ['./tests/']
        args.channels = ['1', '2']
        exp = "./tests/:  {'0001': 409.7443147246781, '0002': 506.15474171872006}\n"
        f = io.StringIO()
        with contextlib.redirect_stdout(f):
            cs.main(args)
        printed = f.getvalue()
        assert printed == exp
